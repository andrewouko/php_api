<?php
abstract class Response{
    protected $request_instance, $response;
    protected function __construct(Request $request, $request_url = null, $method_name = null, array $arguments = null){
        $cuf_array = null;
        if(is_null($request_url)){
            $cuf_array = [$request, $method_name];
        }
        $this->request_instance = $request;
        $this->response = $request->processRequest($request_url, $cuf_array, $arguments);
        if(empty($this->response)) throw new Exception("Response is empty");
        if(!isset($this->response['output'])) throw new Exception("Response is does not include output property");
    }
    abstract function handleResponse();
}