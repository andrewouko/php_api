<?php
abstract class ClientRequest implements Request{
    protected $valid_input;
    protected $required_params;
    protected $optional_params;
    protected $raw_request_input;
    public function __construct(array $required_params, stdClass $request_input = null, array $optional_params = []){
        $this->required_params = $required_params;
        $this->optional_params = $optional_params;
        $this->raw_request_input = $request_input;
        $this->valid_input = JSONRequest::getValidInput($request_input, $this->required_params, $this->optional_params);
    }
    function getRequestData(){
        $request_data = [];
        foreach(array_merge($this->required_params, $this->optional_params) as $param){
            if(isset($this->valid_input->$param) && !is_null($this->valid_input->$param)){
                $request_data[$param] = $this->valid_input->$param;
            }
        }
        return (object) $request_data;
    }
    function processRequest($request_url = null,  array $cuf_array = null, array $cuf_args = null){
        if(is_null($request_url) && is_null($cuf_array)) throw new InvalidArgumentException("A url or call_user_func array is required");
        if(is_null($cuf_array)){
            return Utils::curlPost(json_encode($this->getRequestData()), $request_url);
        } else {
            if(is_null($cuf_args))
                return call_user_func($cuf_array);
            else
                return call_user_func_array($cuf_array, $cuf_args);
        }
    }
}
?>