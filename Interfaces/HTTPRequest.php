<?php
interface HTTPRequest{
    static function getValidInput(stdClass $request_input = null, array $required_params, array $optional_params);
}