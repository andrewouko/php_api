<?php
interface Request{
    function getRequestData();
    function processRequest($request_url = null, array $cuf_array = null);
}