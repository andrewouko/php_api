<?php
require_once('vendor/autoload.php');
function registerAPI($API_DIRECTORY){
    if(!is_dir($API_DIRECTORY)) throw new InvalidArgumentException("API_DIRECTORY must be a directory");
    return spl_autoload_register(function($classname) use ($API_DIRECTORY) {
        $files = preg_grep('/^([^.])/', scandir($API_DIRECTORY));
        foreach($files as $path){
            $path = $API_DIRECTORY.'/'.$path;
            if(is_dir($path)){
                if(file_exists($path.'/'.$classname.'.php')){
                    require_once($path.'/'.$classname.'.php');
                }
            } else {
                if(file_exists($path)){
                    $file_parts = pathinfo($path);
                    if(isset($file_parts['extension']) && $file_parts['extension'] === "php"){
                        require_once($path);
                    }
                }
            }
        }
    });
}
registerAPI(__DIR__);