<?php
define('USER_OPTIONAL_PARAMS', json_encode(['name', 'type', 'id']));
class User extends ClientRequest{
    private $name, $type, $id;
    private static $db;
    function __construct(){
        parent::__construct([], $request_input, json_decode(USER_OPTIONAL_PARAMS, true));
        self::initialiseWorkingCapitalDB();
    }
    private function initUsersTable($users_db_name){
        if(!isset($_ENV['SQL_HOST']) || !isset($_ENV['SQL_USERNAME']) || !isset($_ENV['SQL_PASSWORD']) || !isset($_ENV['USERS_DATABASE'])) throw new Exception("Database credentials are required to initialise the users table");
        self::$db = Database::getInstance($_ENV['SQL_HOST'], $_ENV['SQL_USERNAME'], $_ENV['SQL_PASSWORD']);
        self::$db->createDB($_ENV['USERS_DATABASE']);
        self::$db->selectDatabase($_ENV['USERS_DATABASE']);
        self::$table_fields = [
            'user_id' => 'VARCHAR(64) NOT NULL PRIMARY KEY',
            'type' => 'TINYINT(1) NOT NULL',
            'name' => 'VARCHAR(255) NOT NULL',
            'password' => 'VARCHAR(255) NOT NULL'
        ];
        self::$db->createTable($_ENV['USERS_TABLE'], self::$table_fields);
    }
    private function getID(){
        $config = Utils::getConfig();
        return hash_hmac('sha256', ($this->name . $this->type), bin2hex($config['API_KEY']));
    }
    private function getResponsePayload(){
        return [
            'user_id' => $this->user_id,
            'type' => $this->type,
            'name' => $this->name,
            'password' => $this->password
        ];
    }
    function createUser(){
        try{
            parent::__construct(array_filter(json_decode(USER_OPTIONAL_PARAMS, true), function($param){
                return in_array($param, ['type', 'name', 'password']);
            }), $this->raw_request_input, json_decode(USER_OPTIONAL_PARAMS, true));
        } catch(Exception $e){
            http_response_code(400);
            die(Utils::formatError($e, 'Unable to handle agreement request', true));
        }
        $data = $this->getResponsePayload();
        $db_payload =  $this->getDatabasePayload($data, self::$table_fields);
        $users = self::$db->select($_ENV['USERS_TABLE'], null, (object)['where' => ['user_id' => $this->getID()]]);
        if($users->num_rows < 1){
            self::$db->insert($_ENV['USERS_TABLE'], $db_payload);
            http_response_code(201);
        } else {
            $user = mysqli_fetch_object($loan_request_query->result_obj);
            if($loan->isClosed) throw new Exception("Today's loan is closed");
            if($loan->isApproved){
                $this->initClassPropertiesFromObj($loan);
                $data = $this->getResponsePayload();
                $data['loan_id'] = $this->getLoanID();
                //consideration for 304 should be made
                http_response_code(200);
                Utils::setCacheHeaders(__FILE__, new DateTime($data['approval_datetime']));
            } else {
                self::$db->update($_ENV['LOAN_AGREEMENT_TABLE'], $db_payload, ['loan_id' => $this->getLoanID()]);
            } 
        }
    }
}