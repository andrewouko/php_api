<?php
abstract class UserProvider{
    protected $user_data;
    protected function __construct($user_data){
        if(!is_array($user_data) || !($user_data instanceof stdClass)) throw new Exception("User row must be an array or stdClass object");
        $this->user_data = $user_data;
    }
    protected function getUserDataField(string $prop_name){
        return is_array($this->user_data) ? $this->user_data[$prop_name] : $this->user_data->$prop_name;
    }
    protected function validteUserData(){
        $fields_count = is_array($this->user_data) ? count($this->user_data) : count(json_decode(json_encode($this->user_data), true));
        $fields = is_array($this->user_data) ? array_keys($this->user_data) : array_keys(json_decode(json_encode($user_data), true));
        if(count(array_intersect($fields, ['name', 'role_id'])) == $fields_count) return true;
        return false;
    }
    abstract static function createUser($user_data);
}