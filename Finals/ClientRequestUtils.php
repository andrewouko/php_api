<?php
final class ClientRequestUtils{
    static function getDatabasePayload(array $response_data, $table_fields){
        return array_filter($response_data, function($key) use ($table_fields){
            return array_key_exists($key, $table_fields);
        }, ARRAY_FILTER_USE_KEY);
    }
    static function handleInsert(Database $db, string $table, stdClass $records_filter, array $db_payload){
        $filter_required_props = ['where', 'whereOr'];
        $filter_param_count = array_reduce(array_keys(json_decode(json_encode($records_filter), true)), function($count, $param) use ($filter_required_props) {
            if(in_array($param, $filter_required_props)) $count++;
            return $count;
        }, 0);
        if($filter_param_count < 1) throw new InvalidArgumentException("Records filter must include atleast one of the following props " . json_encode($filter_required_props));
        $records = $db->select($table, null, $records_filter);
        if($records->num_rows < 1){
            $db->insert($table, $db_payload);
            http_response_code(201);
        } else {
            return mysqli_fetch_object($records->result_obj);
        }
    }
}
?>