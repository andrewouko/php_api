<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('Africa/Nairobi');
final class Utils{
    public static function curlGet(string $url, array $data = null, array $headers = null, array $additional_response = null){
        if(count($data) > 0){
            $data = '?' . http_build_query($data);
        }
        $response = self::getRequestInformation($data);
        //init curl
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url . $data);

        /**
         * Check for headers and set if availed
         */
        if(!is_null($headers) &&!empty($headers)){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        //return transfer response as string to the $curl resource
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //follow any 'Location:' header the server sends
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        //output verbose info
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        //ensure its a get request
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_HTTPGET, true);

        $output = curl_exec($curl);

        $response = self::formatNetworkResponse($curl, $output, count($additional_response) > 0 ? array_merge($response, $additional_response) : $response);

        curl_close($curl);

        return $response;
        
    }
    public static function curlPost(string $data, string $url, array $headers = null, array $additional_response = null){
        $response = self::getRequestInformation($data);
        //init curl
        $curl = curl_init();


        /**
         * CURL OPTIONS
         */
        //set url
        curl_setopt($curl, CURLOPT_URL, $url);

        /**
         * Check for headers and set if availed
         */
        if(!is_null($headers) &&!empty($headers)){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        //return transfer response as string to the $curl resource
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        //follow any 'Location:' header the server sends
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        //output verbose info
        curl_setopt($curl, CURLOPT_VERBOSE, true);

        //request method is POST
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        //request body
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($curl);

        $response = self::formatNetworkResponse($curl, $output, count($additional_response) > 0 ? array_merge($response, $additional_response) : $response);

        curl_close($curl);

        return $response;
    }
    public static function getRequestInformation(string $data, array $additional_response = null){
        $response = [
            'request_time' => new DateTime,
            'request_data' => $data,
            'php_server_info' => $_SERVER,
            'php_request_info' => $_REQUEST
        ];
        return (!is_null($additional_response) && count($additional_response) > 0) ? array_merge($response, $additional_response) : $response;
    }
    private static function formatNetworkResponse($curl, $output, array $additional_response = null){
        if(is_string($output) || is_bool($output)){
            $response['output'] = $output;
            $response['curl_info'] = curl_getinfo($curl);
            $response['status'] = true;
            $response['response_time'] = new DateTime();
            if($output == false){
                $response['status'] = false;
                $response['error_code'] = curl_errno($curl);
                $response['error'] = curl_error($curl);
            }
            if(count($additional_response) > 0){
                $response = array_merge($response, $additional_response);
            }
            return $response;
        }
        else{
            throw new InvalidArgumentException('Output argument to formatNetworkResponse must be a string or a boolean');
        }
    }
    static function formatResponse($status, stdClass $output, array $additional_response = null){
        if(!is_bool($status)) throw new InvalidArgumentException("Status argument must be a boolean value");
        $result['output'] = $output;
        $result['status'] = $status;
        $result['response_time'] = new DateTime();
        if(count($additional_response) > 0){
            $result = array_merge($result, $additional_response);
        }
        return $result;
    }
    public static function formatError(Exception $e, $error_desc, $allow_message = false){
        if(!is_dir($_SERVER['DOCUMENT_ROOT'] . $_ENV['LOGS_PATH'] . '/system_error_logs')) throw new Exception("Cannot log error: invalid path combination");
        $message = ((json_decode($e->getMessage()) == null) ? $e->getMessage() : json_decode($e->getMessage()));
        $err_arr = ['Desc' => $error_desc,
            'Line' => $e->getLine(),
            'File' => basename($e->getFile()),
            'Message' => $message,
            'StackTrace' => $e->getTraceAsString(),
            'error_time' => new DateTime()
        ];
        self::logger($err_arr, $_SERVER['DOCUMENT_ROOT'] . $_ENV['LOGS_PATH'] . '/system_error_logs');
        $general_obj = [
            'error' => $err_arr['Desc'],
            'resolution' => 'Kindly contact administrator for more information'
        ];
        if($allow_message){
            $general_obj['resolution'] = $message;
        }
        return json_encode((object) $general_obj);
    }
    public static function validatePhpInput(stdClass $raw_input, array $required_params, array $optional_params){
        $res_obj = null;
        $missing_params = [];
        $existsAndNotEmpty = function($raw_input, $param){
            if(property_exists($raw_input, $param) && (is_bool($raw_input->$param) || is_numeric($raw_input->$param) || !empty($raw_input->$param))){
                return true;
            }
            return false;
        };
        if($raw_input){
            foreach($required_params as $param){
                if($existsAndNotEmpty($raw_input, $param)){
                    $res_obj[$param] = is_string($raw_input->$param) ? urldecode($raw_input->$param) : $raw_input->$param;
                }
                else{
                    array_push($missing_params, $param);
                }
            }
            foreach($optional_params as $param){
                if($existsAndNotEmpty($raw_input, $param))
                    $res_obj[$param] = is_string($raw_input->$param) ? urldecode($raw_input->$param) : $raw_input->$param; 
            }
            if(!empty($missing_params)){
                throw new Exception(json_encode(['missing_params' => $missing_params]));
            }
            return (object) $res_obj;
        }
        else{
            throw new Exception(json_encode(['missing_params' => $required_params]));
            return false;
        }
    }
    public static function logger(array $logs, $dirname){
        if(!is_string($dirname)){
            throw new InvalidArgumentException('dirname must be a string');
        }
        else{
            $logs = json_encode($logs, JSON_PRETTY_PRINT);

            $dir_exists = (is_dir($dirname) && file_exists($dirname) && is_writable($dirname));

            if(!$dir_exists){
                try{
                    if(!mkdir($dirname, 0755, true)){
                        throw new Exception('Unable to create directory');
                    }
                }
                catch(Exception $e){
                    echo $dirname;
                    die('Unable to create log files. Check server permissions');
                }
            }

            file_put_contents($dirname."/".date("Y-m-d").'.log', $logs."\n", FILE_APPEND | LOCK_EX);
        }
    }
    public static function generalAPIErrorMessage($api_name = null){
        if(!is_null($api_name) && !is_string($api_name)){
            throw new InvalidArgumentException('API name must be a string');
        }
        if(!is_null($api_name) && is_string($api_name)){
            $api_name .= ' ';
        }
        return (object)['error' => $api_name.'API error.', 'resolution' => 'Kindly contact administrator.'];
    }
    static function isAbsolutePath($path){
        return substr($path, 0, 1) == '/';
    }
    static function validateLogsPath(){
        if(!isset($_ENV['LOGS_PATH'])) throw new Exception("A valid logs path for the logs must be set");
        if(!self::isAbsolutePath($_ENV['LOGS_PATH'])) throw new Exception("Logs path must be absolute");
        if(!is_dir($_SERVER['DOCUMENT_ROOT'] . $_ENV['LOGS_PATH'])) throw new Exception("Logs Path is not valid directory. Ensure it is relative to the web server root");
        if(substr($_ENV['LOGS_PATH'], -1) == '/') throw new Exception("Logs path cannot end with '/'");
        return true;
    }
    static function setCacheHeaders($file, DateTime $last_content_mod_dt, $duration = 28, $isPublic = true){
        if(!is_file($file)) throw new InvalidArgumentException("A valid file is required");
        $file_last_mod_time = filemtime($file);
        $content_last_mod_time = $last_content_mod_dt->getTimestamp();
        $etag = '"' . $file_last_mod_time . '.' . $content_last_mod_time . '"';
        //max age = seconds in a day * duration in days
        $max_age = 86400 * $duration;
        $public = $isPublic ? 'public' : 'private';
        header('Cache-Control: ' . $public . ', max-age='.$max_age);
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', $content_last_mod_time));
        header('Etag: '.$etag);
    }
    static function writeToConfig($contents, $append = false){
        $config_file = fopen($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['config_file_path'], $append ? 'a+' : 'w+') or die("Unable to open/create config file");
        fwrite($config_file, $contents);
        fclose($config_file);
    }
    static function createAPIKey(){
        $isStrong = false; $secure_key = null;
        while(!$isStrong){
            $secure_key = openssl_random_pseudo_bytes(32, $isStrong);
        }
        return $secure_key;
    }
    static function getConfig(){
        try{
            if(!file_exists($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['config_file_path'])) throw new Exception("Config file does not exist");
        } catch (Exception $e){
            if(!is_writable(dirname($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['config_file_path']))) throw new Exception("Config file path (" . $_SERVER['DOCUMENT_ROOT'] . $GLOBALS['config_file_path'] . ") is not writable, hence cannot be created");
            $handle = fopen($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['config_file_path'], 'w');
            fclose($handle);
            self::setDefaultConfigData();
        }
        $configs = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . $GLOBALS['config_file_path']);
        return $configs;
    }
    private static function setDefaultConfigData(array $data = null){
        if(is_null($data)){
            self::writeToConfig('API_KEY = ' .  '"' . self::createAPIKey() . '"');
        }
    }
    static function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                utf8_encode_deep($value);
            }
            
            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            
            foreach ($vars as $var) {
                utf8_encode_deep($input->$var);
            }
        }
    }
}
?>