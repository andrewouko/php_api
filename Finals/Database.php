<?php
final class Database{
    private $conn;
    static function getInstance($host, $username, $password, $db = null){
        return new Database($host, $username, $password, $db);
    }
    function closeConnection(Database $db){
        mysqli_close($db->conn);
    }
    private function __construct($host, $username, $password, $db = null){
        $this->conn = $this->connect($host, $username, $password);
        if(!is_null($db)){
            $this->selectDatabase($db);
        }
    }
    function tableExists($table_name){
        $sql = 'SELECT 1 FROM ' . $table_name . ' LIMIT 1';
        return $this->getResult($sql, 'table_exists_check');
    }
    function createEnumDataType(array $types){
        $enum_str = 'ENUM(' . array_reduce($types, function($str, $type){
            return $str . "'$type'" . ", ";   
        });
        return substr($enum_str, 0, -2) . ") NOT NULL";
    }
    function selectDatabase($db_name){
        $this->conn->select_db($db_name);
        if($this->conn->error) throw new Exception(json_encode($this->conn->error_list));
    }
    function createDB($db_name){
        $sql = 'CREATE DATABASE IF NOT EXISTS ' . $db_name;
        if(!$this->getResult($sql, 'create_database_if_not_exists')->result_obj){
            throw new Exception('Unable to create database');
        }
    }
    function createTable($table, array $fields){
        if(!is_string($table)){
            throw new InvalidArgumentException('table must be a string');
        }
        $sql = 'CREATE TABLE IF NOT EXISTS ' . $table . '( ';
        foreach($fields as $field => $data_type){
            $sql .= $field . ' ' . $data_type . ', ';
        }
        $sql = substr($sql, 0, -2);
        $sql .= ' )';
        $this->getResult($sql, 'create_table_if_not_exists');
    }
    function createForeignKey($table, stdClass $foreign_key){
        if(($this->select('information_schema.referential_constraints', ['constraint_name'], (object) ['where' => ['constraint_name' => 'fk_' . $table.'_'.$foreign_key->foreign_key]]))->num_rows < 1){
            $sql = 'ALTER TABLE ' . $table;
            foreach(['foreign_key', 'ref_table', 'ref_field'] as $required_prop){
                if(!isset($foreign_key->$required_prop) || empty($foreign_key->$required_prop)) throw new InvalidArgumentException($required_prop . " is required");
            }
            $sql .= ' ADD CONSTRAINT fk_' . $table.'_'.$foreign_key->foreign_key . ' FOREIGN KEY (' . $foreign_key->foreign_key . ')' . ' REFERENCES ' . $foreign_key->ref_table . '(' . $foreign_key->ref_field . ')';
            return $this->getResult($sql, 'add_foreign_key');
        }
        return;
    }
    function dropForeignKey($table, $foreign_key){
        if(!is_string($foreign_key)) throw new InvalidArgumentException("Foreign key must be a string");
        $sql = 'ALTER TABLE ' . $table . ' DROP CONSTRAINT ' . $foreign_key;
        return $this->getResult($sql, 'drop_foreign_key');
    }
    private function connect($host, $username, $password){
        $conn = new mysqli($host, $username, $password);
        if(mysqli_connect_errno()){
            throw new Exception(mysqli_connect_error());
            return false;
        }
        return $conn;
    }
    function insert($table, array $data){
        if(!is_string($table)){
            throw new InvalidArgumentException('parameter table must be a string');
        }
        $sql = 'INSERT INTO  ' . $table . ' ';
        $insert_params = '(';
        $insert_vals = ' values ' . $insert_params;
        foreach($data as $key => $val){
            $insert_params .= ' ' . $key . ', ';
            $insert_vals .= "'" . $val . "',";
        }
        $insert_params = substr($insert_params, 0, -2);
        $insert_params .= ')';
        $insert_vals = substr($insert_vals, 0, -1);
        $insert_vals .= ')';
        $sql = $sql . $insert_params . $insert_vals;
        return $this->getResult($sql, 'insert');
    }
    function insertIfNotExists($table, array $data, array $unique_fields){
        $sql = 'INSERT INTO ' . $table . ' (';
        $fields = $values = '';
        foreach($data as $field => $value){
            $fields .= $field . ', ';
            $values .= "'" . $value . "', ";  
        }
        $fields = substr($fields, 0, -2);
        $values = substr($values, 0, -2);
        $unique_string = $unique_field_string = '';

        foreach($unique_fields as $field => $value){
            $unique_string .= $field . ' = \'' . $value . '\' and ';
            $unique_field_string .= $field . ', ';
        }
        $unique_string = substr($unique_string, 0, -5);
        $unique_field_string = substr($unique_field_string, 0, -2);
        $sql .= $fields . ') SELECT * FROM (SELECT ' . $values . ') AS tmp WHERE NOT EXISTS ( SELECT ' . $unique_field_string . ' FROM ' . $table . ' WHERE ' . $unique_string . ' ) LIMIT 1';

        return $this->getResult($sql, 'insert_if_not_exists');
    }
    function select($table, array $fields = null, stdClass $whereObj = null){
        if(!is_string($table)){
            throw new InvalidArgumentException('parameter table must be a string');
        }
        $sql = 'SELECT ';
        if(!is_null($fields)){
            foreach($fields as $field){
                $sql .= $field . ', ';
            }
            $sql = substr($sql, 0, -2);
        }
        else{
            $sql .= ' *';
        }
        $sql .= ' FROM ' . $table;
        $sql .= $this->handleWhere($whereObj);
        return $this->getResult($sql, 'select');
    }
    private function handleWhere(stdClass $whereObj = null){
        $where = isset($whereObj->where) ? $whereObj->where : null;
        $whereOr = isset($whereObj->whereOr) ? $whereObj->whereOr : null;
        $isAnd = isset($whereObj->isAnd) ? $whereObj->isAnd : null;
        $sql = '';
        if((is_array($where) || is_array($whereOr)) && (!is_null($where) || !is_null($whereOr)))
            $sql .= ' WHERE';
        if(is_array($where) && count($where) > 0){
            $sql .= ' (';
            $where = $this->generateWhereClause($where);
            foreach($where as $clause){
                $sql .= $clause . " AND ";
            }
            $sql = substr($sql, 0, -5) . ')';
        }
        if(is_array($whereOr) && count($whereOr) > 0){
            $sql .= (is_null($where) ? ' ' : (' ' . $isAnd ? 'AND' : 'OR' . ' ')) . '(';
            $whereOr = $this->generateWhereClause($whereOr);
            foreach($whereOr as $clause){
                $sql .= $clause . " OR ";
            }
            $sql = substr($sql, 0, -4) . ')';
        }
        return $sql;
    }
    function update($table, array $fields, stdClass $whereObj = null){
        if(!is_string($table)){
            throw new InvalidArgumentException('parameter table must be a string');
        }
        $sql = 'UPDATE ' . $table . ' SET ';
        if(count($fields) > 0){
            foreach($fields as $key => $val){
                $sql .= $key . "='" . $val . "', ";
            }
        }
        else
            throw new InvalidArgumentException('fields should be an array and have more than one value');
        $sql = substr($sql, 0, -2);
        $sql .= $this->handleWhere($whereObj);
        return $this->getResult($sql, 'update');
    }
    private function generateWhereClause(array $where){
        $res = [];
        foreach($where as $key => $value){
            if(is_string($key)){
                $res[] = $key . " = '" . $value . "'"; 
            } else {
                $res[] = $value;
            }
        }
        return $res;
    }
    private function getResult($sql, $operation_type, $isStoreResult = false){
        $result = $this->conn->query($sql, MYSQLI_STORE_RESULT);
        if($result === true || (isset($result->num_rows) && $result->num_rows >= 0)){
            return $this->formatResult($result, $operation_type);
        } else {
            throw new Exception($this->conn->error . ' ' . $sql);
            return false;
        }
    }
    private function formatResult($result, $operation_type){
        $result_obj = [
            'status' => 'success',
            'operation_type' => $operation_type,
            'affected_rows' => $this->conn->affected_rows,
            'result_obj' => $result
        ];
        if($operation_type == 'select'){
            $result_obj['num_rows'] = $result->num_rows;
        }
        return (object) $result_obj;
    }
}
?>