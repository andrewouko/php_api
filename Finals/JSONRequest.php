<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
final class JSONRequest implements HTTPRequest{
    private static $request_input;
    private static $valid_input;
    private static function sanitizeRequest(stdClass $request_input = null, array $required_params, array $optional_params = []){
        try{
            $post_data = json_decode(file_get_contents('php://input'));
            self::$request_input = (object) [];
            foreach(array_merge($required_params, $optional_params) as $param){
                if(!is_null($request_input) && isset($request_input->$param)){
                    self::$request_input->$param = $request_input->$param;
                    continue;
                }
                else{
                    if(!is_null($post_data) && isset($post_data->$param)){
                        self::$request_input->$param = $post_data->$param;
                    }
                    else{
                        self::$request_input->$param = null;
                    }
                }
            }
            self::$valid_input = Utils::validatePhpInput(self::$request_input, $required_params, $optional_params);
        }
        catch(Exception $e){
            http_response_code(400);
            die(Utils::formatError($e, 'Invalid request format', true));
        }
        catch(InvalidArgumentException $e){
            http_response_code(400);
            die(Utils::formatError($e, 'Invalid argument format', true));
        }
    }
    static function getValidInput(stdClass $request_input = null, array $required_params, array $optional_params = []){
        $pre_validated_input = [];
        foreach(array_merge($required_params, $optional_params) as $param){
            if(isset($request_input->$param) && !is_null($request_input->$param)){
                $pre_validated_input[$param] = $request_input->$param;
            }
        }
        $request_input = (empty($pre_validated_input)) ? null : (object) ($pre_validated_input);
        self::sanitizeRequest($request_input, $required_params, $optional_params);
        return self::$valid_input;
    }
}