<?php
class SimpleProvider extends UserProvider{
    private $name, $role;
    protected function __construct($user_data){
        parent::__construct($user_data);
        $this->name = $this-getUserDataField('name');
        $this->role = new Role($this->getUserDataField('role_id'));
    }
}