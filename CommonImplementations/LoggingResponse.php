<?php
class LoggingResponse extends Response{
    private $log_class;
    public function __construct(Request $request, $request_url = null, $method_name = null, array $arguments = null){
        parent::__construct($request, $request_url, $method_name, $arguments);
        $this->log_class = get_class($request);
    }
    function handleResponse(){
        try{
            Utils::logger($this->response, $_SERVER['DOCUMENT_ROOT'] . $_ENV['LOGS_PATH'] . '/' . $this->log_class);
            return $this->response['output'];
        } catch(Exception $e){
            die(Utils::formatError($e, "Logging response handler failed."));
        }
    }
}